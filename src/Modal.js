var Static = require("Static.js");
var transition = require("css-transition");

var Modal = function( options ) {

	var debug = 0;

	// var eventToPromise = require("../../node_modules/event-to-promise");

	// private properties
	var el;
	var cssClass = "modal";
	var body;
	var buttonEls;
	var targetEl;
	var wrapper;
	var contentEl;
	var contentElContent;
	var backdrop;

	var initialized = false;
	var hidden = true;

	var requiredOptions = [];

	// public api
	var api = {
		options: options,
		destroy: destroy,
		show: show,
		hide: hide,
		getElement: getElement,
		getTarget: getTarget
	}

	function init() {

		if ( debug ) console.log("Modal.init", options);

		options  = options || {};
		cssClass = options.cssClass || cssClass;

		body             = document.body;
		buttonEls        = body.querySelectorAll( options.selectorButtons );
		contentEl        = body.querySelector( options.selectorContent );

		if ( !contentEl || !buttonEls ) {
			return;
		}

		contentElContent = contentEl.querySelector(".content");
		backdrop         = _createBackdrop();

		body.appendChild(backdrop);
		contentEl.classList.add("modal-content");
		contentEl.classList.remove("modal");

		if ( contentEl.parentNode && contentEl.parentNode.classList.contains(cssClass) ) {
			// use existing wrapper
			wrapper = contentEl.parentNode;
		} else {
			// create wrapper
			wrapper = document.createElement("div");
			wrapper.classList.add(cssClass);
			wrapper.appendChild(contentEl);
			body.appendChild(wrapper);
		}
		wrapper.modal = api; // TODO this can overwrite existing reference of modal instance... only downside: open modal is not automatically closed when another one is shown

		_activate();

		_onInitialized();
	}

	function destroy() {

		if ( initialized ) {

			hide();
			_deactivate();
			_finalize();
		}
	}

	function show( html ) {

		if ( !hidden ) {
			return;
		}

		_onBeforeShow();

		hidden = false;

		backdrop.style.display = "block";
		transition( backdrop, {opacity: 0.8}, 300, _onShow )

		if ( document.querySelector(cssClass+".show") ) {
			document.querySelector(cssClass+".show").modal.hide();
		}

		if ( html && contentElContent ) {
			contentElContent.innerHTML = html;
		}

		wrapper.classList.add("show");
		body.style.overflow = "hidden";

		setTimeout(function() {
			contentEl.classList.add("show");
			wrapper.addEventListener("click", _onWrapperClick, true);
		});

	}

	function hide() {

		if ( hidden ) {
			return;
		}

		_onBeforeHide();

		hidden = true;

		transition( backdrop, {opacity: 0}, 300).then(function() {
			backdrop.style.display = "";
		});

		body.style.overflow = "";
		contentEl.style.opacity = "1";
		contentEl.classList.remove("show");
		transition( contentEl, {opacity: 0}, 300).then(function () {
			_onHide();
			contentEl.style.opacity = "";
		});

		document.removeEventListener("click", _onWrapperClick, true);
	}

	function getElement() {

		return contentEl;
	}

	function getTarget() {

		return targetEl;
	}

	// private

	function _activate() {

		if ( debug ) console.log("Modal._activate", buttonEls);

		document.addEventListener("onAfterDomChange", _onAfterDomChange);

		Static.each(buttonEls, function (element) {
			element.addEventListener("click", _onButtonClick);
		});
	}

	function _deactivate() {

		document.removeEventListener("onAfterDomChange", _onAfterDomChange);

		Static.each(buttonEls, function (element) {
			element.removeEventListener("click", _onButtonClick);
		});
	}

	function _finalize() {

		wrapper.parentNode.removeChild(wrapper);
		backdrop.parentNode.removeChild(backdrop);
	}

	function _onAfterDomChange( event ) {

		if ( debug ) console.log("Modal._onAfterDomChange", event);

		buttonEls = body.querySelectorAll( options.selectorButtons );
		_deactivate();
		_activate();
	}

	function _createBackdrop() {

		var el = document.createElement("div");
			el.classList.add("modal-backdrop");
		return el;
	}

	function _onWrapperClick( e ) {

		if ( debug ) console.log("Modal._onWrapperClick", e );

		if ( !hidden ){

			var clickedOutside = e.target == wrapper;
			var clickedClose   = e.target.classList.contains("close") && contentEl.contains(e.target);

			if ( clickedOutside || clickedClose ){

				hide();
			}
		}
	}

	function _onInitialized() {

		initialized = true;
		wrapper.classList.add(cssClass+"-initialized");

		if ( typeof options.onInitialized == "function" ){
			options.onInitialized.call( api, api );
		}
	}

	function _onBeforeShow() {

		if ( debug ) console.log("Modal._onBeforeShow", options.onBeforeShow);
		if ( typeof options.onBeforeShow == "function" ){
			options.onBeforeShow.call( api, api );
		}
	}

	function _onShow() {

		if ( debug ) console.log("Modal._onShow", backdrop );

		if ( typeof options.onShow == "function" ){
			options.onShow.call( api, api );
		}
	}

	function _onBeforeHide() {


		if ( typeof options.onBeforeHide == "function" ){
			options.onBeforeHide.call( api, api );
		}
	}

	function _onHide() {

		if ( debug ) console.log("Modal._onHide");

		wrapper.classList.remove("show");

		if ( typeof options.onHide == "function" ){
			options.onHide.call( api, api );
		}
	}

	function _onDomChange() {

		if ( options && typeof options.onDomChange == "function" ){
			options.onDomChange();
		}
	}

	function _onButtonClick(e) {

		e.preventDefault();

		targetEl = e.target;

		show();
	}

	return init(), api;
}

if ( typeof module == "object" ) {
	module.exports = Modal;
}

